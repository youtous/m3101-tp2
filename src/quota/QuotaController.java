package quota;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;

import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutionException;

public class QuotaController {
    @FXML
    private Label progressLabel;
    @FXML
    private ProgressBar progressBar;
    @FXML
    private Button runButton;
    @FXML
    private ListView<FileItem> fileList;

    public void onRun() throws ExecutionException, InterruptedException {


        this.progressLabel.setText("Traitement en cours...");
        this.progressBar.setProgress(-1);
        this.runButton.setDisable(true);

        //DirectoryLister directoryLister = new DirectoryLister(System.getProperty("user.home"));
        DirectoryLister directoryLister = new DirectoryLister("/Users/youtous/ownCloud");

        long total = 282422567;
        // Liste la racine utilisateur
        Main.executorService.submit(new Runnable() {
            @Override
            public void run() {
                directoryLister.addObserver(new Observer() {
                    long totalSize;

                    @Override
                    public void update(Observable o, Object arg) {
                        ProgressEvent progressEvent = (ProgressEvent) arg;

                        totalSize += progressEvent.getTotalSize();


                        Platform.runLater(() -> {
                            progressBar.setProgress((float) totalSize / total);
                            progressLabel.setText("" + totalSize / (1024 * 1024) + " Mio" + "(" + (totalSize / (1000 * 1000)) + "Mo)" + "/" + total / (1024 * 1024) + " Mio");
                            fileList.getItems().add(progressEvent.getFileItem());
                        });

                    }
                });
                directoryLister.list();
                Platform.runLater(() -> {
                    fileList.getItems().sort(new Comparator<FileItem>() {
                        @Override
                        public int compare(FileItem o1, FileItem o2) {
                            return -(o1.getSize() - o2.getSize());
                        }
                    });
                });
            }
        });
        runButton.setDisable(false);
    }
}
