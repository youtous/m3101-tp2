package quota;

/**
 * Created by youtous on 21/11/2016.
 */
public class FileItem {
    private String name;
    private int size;

    public FileItem(String name, int size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "[" + size + " Mio] " + name;
    }
}
