package quota;

import java.io.File;
import java.util.Observable;

/**
 * Created by youtous on 14/11/2016.
 */
public class DirectoryLister extends Observable {
    private File startingDirectory;

    public DirectoryLister(String startingDirectoryPath) {
        this.startingDirectory = new File(startingDirectoryPath);
    }

    public Long list() {
        return recursiveList(startingDirectory, "");
    }

    public static void main(String[] args) {
        DirectoryLister lister = new DirectoryLister("/Users/youtous/Pictures/");
        System.out.println(lister.list() / (1024 * 1024) + " Mio");
    }

    private long recursiveList(File file, String indent) {
        long size = file.length();
        if (file.isDirectory()) {
            System.out.print(indent + " + " + file.getName() + "\n");
            File[] files = file.listFiles();
            indent += "   ";
            for (File childFile : files) {
                size += this.recursiveList(new File(childFile.getAbsolutePath()), indent);
            }
        } else {
            System.out.print(indent + "   " + file.getName() + "\n");
            this.setChanged();
            this.notifyObservers(new ProgressEvent(size, new FileItem(file.getAbsolutePath(), (int) (size / (1024 * 1024)))));
        }
        System.out.println(size);
        return size;
    }
}
