package quota;

/**
 * Created by youtous on 15/11/2016.
 */
public class ProgressEvent {
    private final long total;
    private final FileItem fileItem;

    public FileItem getFileItem() {
        return fileItem;
    }

    public ProgressEvent(long total, FileItem fileItem) {
        this.total = total;
        this.fileItem = fileItem;
    }

    long getTotalSize() {
        return this.total;
    }
}
